class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.string :reason
      t.integer :patient_id
      t.integer :physician_id
      t.date :date
      t.string :hour
      t.string :comment
      t.integer :diagnostic_id
      t.string :done

      t.timestamps
    end
  end
end
