class Appointment < ActiveRecord::Base
  before_save { |appointment| appointment.datetime = appointment.datetime.utc }

  belongs_to :diagnostic
  belongs_to :physician
  belongs_to :patient


  validates_uniqueness_of :datetime, scope: [:physician_id]
  validates_uniqueness_of :datetime, scope: [:patient_id]
end
