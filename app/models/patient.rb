class Patient < ActiveRecord::Base
  validate :bar_exists, :on => [ :create]
  has_one :user , :class_name => "User"
  has_many :physician, :through =>  :appointments
  has_many :appointment
  belongs_to :state
  belongs_to :insurance


  def bar_exists
    if Patient.where(user_id: self.user_id).exists? then

      errors.add(:user_id, alert: "email is already assigned to a patient")
    end

  end

end
