class State < ActiveRecord::Base
  has_many :employee
  has_many :patient
  has_many :physician
end
