class Physician < ActiveRecord::Base
  validate :bar_exists, :on => [ :create]
  has_one :user , :class_name => "User"
  has_many :appointment
  has_many :patient, :through =>  :appointments
  belongs_to :state

  def bar_exists
    if Physician.where(user_id: self.user_id).exists? then

      errors.add(:user_id, alert: "email is already assigned to a Physician")
    end
  end
end
