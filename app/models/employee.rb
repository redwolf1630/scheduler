class Employee < ActiveRecord::Base
  validate :bar_exists
  has_one :user , :class_name => "User"
  belongs_to :state


  def bar_exists
    if Employee.where(user_id: self.user_id).exists? then

      errors.add(:user_id, alert:"email is already assigned to a Employee")
    end

  end
end
