json.array!(@appointments) do |appointment|
  json.extract! appointment, :id, :reason, :patient_id, :physician_id, :date, :hour, :comment, :diagnostic_id, :done
  json.url appointment_url(appointment, format: :json)
end
