json.extract! @appointment, :id, :reason, :patient_id, :physician_id, :date, :hour, :comment, :diagnostic_id, :done, :created_at, :updated_at
