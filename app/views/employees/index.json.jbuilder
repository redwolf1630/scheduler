json.array!(@employees) do |employee|
  json.extract! employee, :id, :address, :city, :state_id, :zipcode, :phone, :user_id, :ssn
  json.url employee_url(employee, format: :json)
end
