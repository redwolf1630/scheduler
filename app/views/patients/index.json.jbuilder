json.array!(@patients) do |patient|
  json.extract! patient, :id, :adress, :city, :state_id, :zipcode, :phone, :insurance_id, :user_id
  json.url patient_url(patient, format: :json)
end
