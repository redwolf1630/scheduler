json.array!(@insurances) do |insurance|
  json.extract! insurance, :id, :name, :coverage
  json.url insurance_url(insurance, format: :json)
end
