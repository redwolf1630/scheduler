class DiagnosticsController < ApplicationController
  before_action :set_diagnostic, only: [:show, :edit, :update, :destroy]
  before_filter :admin_only
  respond_to :html

  def index
    @diagnostics = Diagnostic.all.page params[:page]
    respond_with(@diagnostics)
  end

  def show
    respond_with(@diagnostic)
  end

  def new
    @diagnostic = Diagnostic.new
    respond_with(@diagnostic)
  end

  def edit
  end

  def create
    @diagnostic = Diagnostic.new(diagnostic_params)
    @diagnostic.save
    respond_with(@diagnostic)
  end

  def update
    @diagnostic.update(diagnostic_params)
    respond_with(@diagnostic)
  end

  def destroy
    @diagnostic.destroy
    respond_with(@diagnostic)
  end

  private
    def set_diagnostic
      @diagnostic = Diagnostic.find(params[:id])
    end

    def diagnostic_params
      params.require(:diagnostic).permit(:code, :description, :fee)
    end
  def admin_only
    unless current_user.admin?
      redirect_to(:root,alert: "Access denied")
    end
  end

end
