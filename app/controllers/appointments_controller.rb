class AppointmentsController < ApplicationController
  before_action :set_appointment, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @appointments = Appointment.all.order(:done).page params[:page]

    respond_with(@appointments)
  end

  def show
    respond_to do |format|
      format.html
      format.pdf do
        render :pdf => "appointment"
      end
    end

  end

  def new
    @selecteddate = params[:currdate]
    @appointment = Appointment.new
    respond_with(@appointment)
  end

  def edit
  end

  def create
    @appointment = Appointment.new(appointment_params)
    @appointment.save
    respond_with(@appointment)
  end

  def update
    @selecteddate = params[:currdate]
    @appointment.update(appointment_params)
    respond_with(@appointment)
  end

  def destroy

    @appointment.destroy
    respond_to do |format|
      format.html { redirect_to appointments_url, notice: 'Appointment was successfully destroyed.' }
      end
    respond_with(@appointment)
  end

  private
    def set_appointment
      @selecteddate = params[:currdate]
      @appointment = Appointment.find(params[:id])
    end
  def patientAndPhysician_only
    unless current_user.patient? || current_user.physician? || current_user.employee? || current_user.admin?
      redirect_to(:root,alert: "Access denied")
    end

  end
    def appointment_params
      params.require(:appointment).permit(:reason, :patient_id, :physician_id, :date, :hour, :datetime, :comment, :diagnostic_id, :done)
    end
end
