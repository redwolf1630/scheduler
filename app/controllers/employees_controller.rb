class EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :edit, :update, :destroy]
  before_filter :emp_only
  respond_to :html

  def index
    @employees = Employee.all.page params[:page]
    respond_with(@employees)
  end

  def show
    respond_with(@employee)
  end

  def new
    @employee = Employee.new
    respond_with(@employee)
  end

  def edit
  end

  def create
    @employee = Employee.new(employee_params)
    @employee.save
    respond_with(@employee)
  end

  def update
    @employee.update(employee_params)
    respond_with(@employee)
  end

  def destroy
    @employee.destroy
    respond_with(@employee)
  end

  private
    def set_employee
      @employee = Employee.find(params[:id])
    end

    def employee_params
      params.require(:employee).permit(:address, :city, :state_id, :zipcode, :phone, :user_id, :ssn, :name)
    end

  def emp_only
    unless current_user.employee? || current_user.admin?
      redirect_to(:root,alert: "Access denied")
    end
  end
end
