class PatientsController < ApplicationController
  before_action :set_patient, only: [:show, :edit, :update, :destroy]
  before_filter :patient_only
  respond_to :html

  def index
    @patients = Patient.all.page params[:page]
    respond_with(@patients)
  end

  def show
    respond_with(@patient)
  end

  def new
    @patient = Patient.new
    respond_with(@patient)
  end

  def edit
  end

  def create
    @patient = Patient.new(patient_params)
    @patient.save
    respond_with(@patient)
  end

  def update
    @patient.update(patient_params)
    respond_with(@patient)
  end

  def destroy
    @patient.destroy
    respond_with(@patient)
  end

  private
    def set_patient
      @patient = Patient.find(params[:id])
    end

    def patient_params
      params.require(:patient).permit(:adress, :city, :state_id, :zipcode, :phone, :insurance_id, :user_id, :name)
    end


  def patient_only
    unless current_user.patient? || current_user.admin?
      redirect_to(:root,alert: "Access denied")
    end
  end

end
