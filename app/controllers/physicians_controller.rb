class PhysiciansController < ApplicationController
  before_action :set_physician, only: [:show, :edit, :update, :destroy]
  before_filter :physician_only
  respond_to :html

  def index
    @physicians = Physician.all.page params[:page]
    respond_with(@physicians)
  end

  def show
    respond_with(@physician)
  end

  def new
    @physician = Physician.new
    respond_with(@physician)
  end

  def edit
  end

  def create
    @physician = Physician.new(physician_params)
    @physician.save
    respond_with(@physician)
  end

  def update
    @physician.update(physician_params)
    respond_with(@physician)
  end

  def destroy
    @physician.destroy
    respond_with(@physician)
  end

  private
    def set_physician
      @physician = Physician.find(params[:id])
    end

    def physician_params
      params.require(:physician).permit(:address, :city, :state_id, :zipcode, :phone, :user_id, :ssn, :name)
    end
  def physician_only

    unless current_user.physician? || current_user.admin?
      redirect_to(:root,alert: "Access denied")
    end
  end
end
