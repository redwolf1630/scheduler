Rails.application.routes.draw do
  resources :appointments

  resources :employees

  resources :physicians

  resources :patients

  resources :diagnostics

  resources :insurances

  resources :states


  mount Upmin::Engine => '/admin'
  root to: 'visitors#index'
  devise_for :users
  resources :users
end
